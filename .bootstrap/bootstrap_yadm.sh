#!/usr/bin/bash

YADM_REPO="https://github.com/TheLocehiliosan/yadm"
YADM_RELEASE="master"
YADM_DIR="$HOME/.config/yadm"
DOTFILES_REPO="https://gitlab.com/brlodi/dotfiles"

function remote_yadm() {
  curl -fsSL "$YADM_REPO/raw/$YADM_RELEASE/yadm" | bash -s -- -Y $YADM_DIR "$@"
}

if [ ! type git > /dev/null 2>&1 ] || [ ! xcode-select -p > /dev/null 2>&1 ]; then
  echo "ERROR: Git is not available. Please ensure it is installed and on your"
  echo "path before running this program again."
  exit 1
fi

remote_yadm clone --no-bootstrap $DOTFILES_REPO
remote_yadm submodule update --init --recursive
remote_yadm bootstrap
