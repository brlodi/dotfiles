#!/bin/bash

function detect_distro {
  if [ -f /etc/os-release ]; then
    . /etc/os-release
    echo $NAME
  elif type lsb_release >/dev/null 2>&1; then
    echo $(lsb_release -si)
  elif [ -f /etc/lsb-release ]; then
    . /etc/lsb-release
    echo $DISTRIB_ID
  else
    echo "unknown"
  fi
}

function setup_debian {
  deb_src="deb http://ppa.launchpad.net/ansible/ansible/ubuntu trusty main"
  echo "$deb_src" | sudo tee -a /etc/apt/sources.list > /dev/null
  sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 93C4A3FD7BB9C367
  sudo apt-get -qq update
  sudo apt-get -yq install ansible
}

function setup_ubuntu {
  sudo apt-get -qq update
  sudo apt-get -y install software-properties-common
  sudo apt-add-repository --yes --update ppa:ansible/ansible
  sudo apt-get -yq install ansible
}

function setup_fedora {
  sudo dnf -yq install ansible
}

function setup_rhel {
  sudo yum -yq install python3
}

function setup_arch {
  sudo pacman -Syu ansible
}

function setup_macos {
  if ! type pip > /dev/null 2>&1; then 
      sudo /bin/bash -c 'curl -fsSL https://bootstrap.pypa.io/get-pip.py | python'
  fi
  pip install --user ansible
}

sudo -vp "Installing Ansible requires sudo access. Password:"
case "$OSTYPE" in
  linux*)
    echo "Detected OS: Linux"
    DISTRO=$(detect_distro)
    echo "Detected distro: $DISTRO"
    
    case "$DISTRO" in
      Debian*) setup_debian;;
      Ubuntu*) setup_ubuntu;;
      Fedora*) setup_fedora;;
      RHEL* | CentOS* | Scientific*) setup_rhel;;
      Arch* | Antergos* | Manjaro*) setup_arch;;
      *)
        echo "ERROR: We don't know how to install Ansible on '$DISTRO'!"
        echo "You'll have to do it manually. Exiting."
    esac
    ;;
  darwin*)
    echo "Detected OS: macOS"
    setup_macos
    ;;
  bsd*)
    echo "Detected OS: BSD"
    sudo pkg install py36-ansible
    ;;
  *)
    echo "ERROR: unknown OS ($OSTYPE)! Exiting." ;;
esac
