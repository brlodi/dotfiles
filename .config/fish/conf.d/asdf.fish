set ASDF_DIR "$HOME/.local/bin/asdf"

if test ! -d "$ASDF_DIR"
  "WARNING: asdf is not installed. Did you initialize your submodules?"
else
  source "$ASDF_DIR/asdf.fish"
  mkdir -p "$XDG_CONFIG_HOME/fish/completions"
  cp "$ASDF_DIR/completions/asdf.fish" "$XDG_CONFIG_HOME/fish/completions"
end
