# -----------------------------------------------------------------------------
#  Comply with XDG
# -----------------------------------------------------------------------------
set -q XDG_DATA_HOME[1]; or set -xg XDG_DATA_HOME "$HOME/.local/share"
set -q XDG_DATA_DIRS[1]; or set -xg XDG_DATA_DIRS "/usr/local/share/:/usr/share/"
set -q XDG_CONFIG_HOME[1]; or set -xg XDG_CONFIG_HOME "$HOME/.config"
set -q XDG_CONFIG_DIRS[1]; or set -xg XDG_CONFIG_DIRS "/etc/xdg"
set -q XDG_CACHE_HOME[1]; or set -xg XDG_CACHE_HOME "$HOME/.cache"

# asdf
set -xg ASDF_DEFAULT_TOOL_VERSIONS_FILENAME "$XDG_CONFIG_HOME/asdf/global-tool-versions"
# aws
set -xg AWS_CONFIG_FILE "$XDG_CONFIG_HOME/aws/config"
set -xg AWS_SHARED_CREDENTIALS_FILE "$XDG_CONFIG_HOME/aws/credentials"
# Ruby
set -xg BUNDLE_USER_CONFIG "$XDG_CONFIG_HOME/bundle"
set -xg BUNDLE_USER_CACHE "$XDG_CACHE_HOME/bundle"
set -xg BUNDLE_USER_PLUGIN "$XDG_DATA_HOME/bundle"
set -xg GEM_HOME "$XDG_DATA_HOME/gem"
set -xg GEM_SPEC_CACHE "$XDG_CACHE_HOME/gem"
# Docker
set -xg DOCKER_CONFIG "$XDG_CONFIG_HOME/docker"
# GNUPG
set -xg GNUPGHOME "$XDG_DATA_HOME/gnupg"
# Gradle
set -xg GRADLE_USER_HOME "$XDG_DATA_HOME/gradle"
# Less
set -xg LESSKEY "$XDG_CONFIG_HOME/less/lesskey"
set -xg LESSHISTFILE "$XDG_CACHE_HOME/less/history"
# NPM
set -xg NPM_CONFIG_USERCONFIG "$XDG_CONFIG_HOME/npm/npmrc"
# Vagrant
set -xg VAGRANT_HOME "$XDG_DATA_HOME/vagrant"
set -xg VAGRANT_ALIAS_FILE "$XDG_DATA_HOME/vagrant/aliases"
# VIM
set -xg VIMINIT 'let $MYVIMRC="$XDG_CONFIG_HOME/vim/vimrc" | source $MYVIMRC'
# xorg-auth
set -xg XAUTHORITY "$XDG_RUNTIME_DIR/Xauthority"
