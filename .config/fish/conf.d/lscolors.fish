# Set colors used by the `ls` command, in BSD-world and GNU-land respectively
set -xg LSCOLORS "ExgxFxDxbxxfxdxbxbxeGe"
set -xg LS_COLORS "di=01;34:ln=36:so=01;35:pi=01;33:ex=31:bd=00;45:cd=00;43:su=00;41:sg=00;41:tw=01;00;44:ow=01;36;44:"
