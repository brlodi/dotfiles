function git_root -d "Return the root of the current git project, even in a submodule"
  if git rev-parse --is-inside-work-tree > /dev/null 2>&1
    set -l spwt (git rev-parse --show-superproject-working-tree)
    if test -n "$spwt"
      echo "$spwt"
    else
      git rev-parse --show-toplevel
    end
  else
    return 1
  end
end
