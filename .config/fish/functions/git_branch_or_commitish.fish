function git_branch_or_commitish -d 'Print the name of the active Git branch or commit-hash-like ID if not on a proper branch'
  git symbolic-ref -q --short HEAD
  or echo (git show-ref --head -s --abbrev)[1]
end

