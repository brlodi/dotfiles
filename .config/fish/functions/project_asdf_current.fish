function project_asdf_current -a plugin_name -d "Return only the ASDF versions set in the current Git project"
  if not is_git_repo; return; end

  set -l files
  set -l dir (pwd)
  set -l parent_dir (dirname (git_root))
  
  # Check every directory up the hierarchy for a .tool-versions file
  while test "$dir" != "$parent_dir"
    if test -f "$dir/.tool-versions"
      set -a files "$dir/.tool-versions"
    end
    set dir (dirname "$dir")
  end

  # Collapse duplicate entries per plugin, preserving the version set closest
  # to the PWD
  set -l plugin_version_array (for f in $files;cat "$f";end | sort -u -k1,1)

  # If a plugin name was specified, output the bare version number (or nothing
  # if no version is set). Otherwise, output the full list of declarations
  if set -q plugin_name[1]
    for entry in $plugin_version_array
      if string match -q "$plugin_name*" "$entry"
        echo (string split " " "$entry")[2]
      end
    end
  else
    for entry in $plugin_version_array;echo "$entry";end
  end
end
