function is_git_repo -d 'Returns a 0 status if in a Git tree, non-0 otherwise'
  git rev-parse --is-inside-work-tree > /dev/null 2>&1
end

