function cdgr -d "Change Directory to root of git working tree if in a repo"
  if git_root > /dev/null
    cd (git_root)
  end
end
