# Color reference
# fish_color_normal,         the default color
# fish_color_command,        the color for commands
# fish_color_quote,          the color for quoted blocks of text
# fish_color_redirection,    the color for IO redirections
# fish_color_end,            the color for process separators like ';' and '&'
# fish_color_error,          the color used to highlight potential errors
# fish_color_param,          the color for regular command parameters
# fish_color_comment,        the color used for code comments
# fish_color_match,          the color used to highlight matching parenthesis
# fish_color_selection,      the color used when selecting text (in vi visual mode)
# fish_color_search_match,   used to highlight history search matches and the selected pager item (must be a background)
# fish_color_operator,       the color for parameter expansion operators like '*' and '~'
# fish_color_escape,         the color used to highlight character escapes like '\n' and '\x70'
# fish_color_cwd,            the color used for the current working directory in the default prompt
# fish_color_autosuggestion, the color used for autosuggestions
# fish_color_user,           the color used to print the current username in some of fish default prompts
# fish_color_host,           the color used to print the current host system in some of fish default prompts
# fish_color_cancel,         the color for the '^C' indicator on a canceled command


function __update_project_asdf_current -S --on-event fish_postexec -a cmdline
  switch "$cmdline"
    case 'cd*' 'asdf*' '*.tool-versions*'
      set -g __prompt_asdf_current (project_asdf_current)
  end
end

function fish_prompt
  ############################################################################
  #                             UTILIY FUNCTIONS                             #
  ############################################################################
  
  function __glyph -a name -d 'Print glyph by name'
    switch "$name"
      # Tools & Frameworks
      case 'elixir';           printf '\uE62D '
      case 'java';             printf '\uF0F4 '
      case 'node*';            printf '\uE24F '
      case 'python';           printf '\uE235 '
      case 'ruby';             printf '\uE23E '
      case 'swift';            printf '\uFBE3 '

      # VCS
      case 'branch';           printf '\uF418 '
      case 'status_ahead';     printf '\uF0DE'
      case 'status_behind';    printf '\uF0DD'
      case 'status_diverged';  printf '\uF0DC'
      case 'status_dirty';     printf '*'
      case 'status_untracked'; printf '?'
      case 'status_staged';    printf '~'

      # Misc.
      case 'prompt_arrow';     printf '\uF178 '
      case 'user';             printf '\uF007'
      case 'computer';         printf '\uF878'
    end
  end

  function __tool_color -a tool -d 'Translate tool name to associated color'
    switch "$tool"
      case 'elixir'; echo -n 'magenta'
      case 'java';   echo -n 'brred'
      case 'node*';  echo -n 'green'
      case 'python'; echo -n 'cyan'
      case 'ruby';   echo -n 'red'
      case 'swift';  echo -n 'brred'
    end
  end

  function __prompt_show_user
    if test (id -u) -eq 0
      or test "x$PROMPT_SHOW_USER" = 'xalways'
      or begin
          test "x$PROMPT_SHOW_USER" = 'xremote'
          and is_remote_session
        end
      or begin
          test "x$PROMPT_SHOW_USER" != 'xnever'
          and test "x$USER" != "x$PROMPT_DEFAULT_USER"
        end
      return 0
    end
    return 1
  end

  function __prompt_show_host
    if test "x$PROMPT_SHOW_HOST" = 'xalways'
      or begin
          test "x$PROMPT_SHOW_HOST" = 'xremote'
          and is_remote_session
        end
      return 0
    end
    return 1
  end

  function __prompt_git_status -S
    set -l is_ahead
    set -l is_behind
    set -l has_modified
    set -l has_untracked
    set -l has_staged

    set -l git_status_porcelain (git status --porcelain -b)

    if string match -q '*ahead*' $git_status_porcelain[1]
      set is_ahead true
    end
    if string match -q '*behind*' $git_status_porcelain[1]
      set is_behind true
    end

    for entry in $git_status_porcelain
      if string match -q --regex '^\s*[MRD]' $entry
        set has_modified true
      end
      if string match -q --regex '^[AMRD]' $entry
        set has_staged true
      end
      if string match -q --regex '^\?\?' $entry
        set has_untracked true
      end
    end

    if test "$is_ahead" = "true" -a "$is_behind" = true
      echo -n (__glyph 'status_diverged')
    else if test "$is_ahead"
      echo -n (__glyph 'status_ahead')
    else if test "$is_behind"
      echo -n (__glyph 'status_behind')
    end

    if test "$has_staged" = "true"
      echo -n (__glyph 'status_staged')
    end
    if test "$has_modified" = "true"
      echo -n (__glyph 'status_dirty')
    end
    if test "$has_untracked" = "true"
      echo -n (__glyph 'status_untracked')
    end
  end

  function __prompt_section_username_host -S
    if not __prompt_show_user; and not __prompt_show_host
      return
    end

    if __prompt_show_user
      set_color normal
      if test (id -u) -eq 0
        # Running as roo
        set_color red --bold
      else
        # Normal user
        set_color "$fish_color_user" --bold
      end
      echo -n "$USER"
    end

    if __prompt_show_user; and __prompt_show_host
      set_color normal
      set_color --dim
      echo -n "@"
    end

    if __prompt_show_host
      set_color normal
      set_color $fish_color_host --bold
      if test "x$PROMPT_HOST_FQDN" = 'xtrue'
        echo -n $hostname
      else
        echo -n (prompt_hostname)
      end
    end

    if __prompt_show_user
      set_color normal
      set_color --dim
      echo -n ' in '
    else if __prompt_show_host
      set_color normal
      echo -n ':'
    end
  end

  function __prompt_section_pwd
    set -l prompt_segments (string split -n '/' (prompt_pwd))
    set -l color $fish_color_cwd

    set_color normal
    if test "~" != "$prompt_segments[1]"
      if test (count $prompt_segments) -eq 0
        set_color $color --bold
      end
      echo -n '/'
    end
    set_color normal
    set_color $color
    for seg in $prompt_segments[1..-2]
      echo -ns $seg '/'
    end
    set_color "$color" --bold
    echo -n $prompt_segments[-1]
  end

  function __prompt_section_git -S
    set_color normal
    set_color --dim
    echo -n ' on '
    set_color normal
    set_color $fish_color_git --bold
    echo -ns (__glyph 'branch') (git_branch_or_commitish)

    set -l git_status (__prompt_git_status)
    if test -n "$git_status"
      set_color normal
      set_color $fish_color_git
      echo -n ' ['
      set_color $fish_color_git --bold
      echo -n "$git_status"
      set_color normal
      set_color $fish_color_git
      echo -n ']'
    end
  end

  function __prompt_section_tool_versions -S
    if test -z "$__prompt_asdf_current"; return; end

    set_color normal
    set_color --dim
    echo -ns ' with '
    set_color normal

    if test (count $__prompt_asdf_current) -gt 1
      for line in $__prompt_asdf_current[1..-2]
        set -l tool (string split ' ' $line)[1]
        set -l tool_ver (string split ' ' $line)[2]

        set_color (__tool_color "$tool")
        echo -ns (__glyph "$tool") $tool_ver
        set_color normal
        echo -ns ', '
      end
    end

    set -l tool (string split ' ' $__prompt_asdf_current[-1])[1]
    set -l tool_ver (string split ' ' $__prompt_asdf_current[-1])[2]

    set_color (__tool_color "$tool")
    echo -ns (__glyph "$tool") $tool_ver
    set_color normal

  end

  function __prompt_section_end -S
    set_color normal
    echo
    if test "$last_cmd_status" -ne 0
      set_color $fish_color_error
    end
    echo -ns (__glyph 'prompt_arrow')
  end

  ############################################################################
  #                                  PROMPT                                  #
  ############################################################################
  
  # Capture and store the exit status of the last command before we run any
  # other commands and obliterate it
  set -l last_cmd_status "$status"

  # Establish default colors if user has not set them globally
  set -q fish_color_user; or set -l fish_color_user blue
  set -q fish_color_host; or set -l fish_color_host green
  set -q fish_color_cwd;  or set -l fish_color_cwd  white
  set -q fish_color_git;  or set -l fish_color_git  brred

  # Default behavior variables
  set -q PROMPT_GIT_PWD_DIR_LENGTH; or set -l PROMPT_GIT_PWD_DIR_LENGTH 3
  set -q PROMPT_SPACING; or set -l PROMPT_SPACING true

  # Ensure ASDF versions are up to date
  if not set -q __prompt_asdf_current
    __update_project_asdf_current
  end

  if test "x$PROMPT_SPACING" = "xtrue"
    echo
  end

  __prompt_section_username_host

  __prompt_section_pwd

  # If in a Git project, display branch and status information
  if is_git_repo
    __prompt_section_git
    __prompt_section_tool_versions
  end

  __prompt_section_end

  ############################################################################
  #                                 CLEANUP                                  #
  ############################################################################
  functions -e __glyph
  functions -e __tool_color
  functions -e __prompt_show_user
  functions -e __prompt_show_host
  functions -e __prompt_git_status
  functions -e __prompt_section_username_host
  functions -e __prompt_section_pwd
  functions -e __prompt_section_git
  functions -e __prompt_section_tool_versions
  functions -e __prompt_section_end

  set_color normal
  return $last_cmd_status
end
