function fish_asdf_current -a plugin_name -d "Equivalent to `asdf current` without legacy file support, but ~33x faster"
  set -l files
  set -l dir (pwd)
  
  # Check every directory up the hierarchy for a .tool-versions file
  while test "$dir" != "/"
    if test -f "$dir/.tool-versions"
      set -a files "$dir/.tool-versions"
    end
    set dir (dirname "$dir")
  end

  # If our file list is empty, fall back to the .tool-versions in the user's
  # home directory
  if ! set -q files[1] -a -f "$HOME/.tool-versions"
    set -a files "$HOME/.tool-versions"
  end
  
  # Collapse duplicate entries per plugin, preserving the version set closest
  # to the PWD
  set -l plugin_version_array (for f in $files;cat "$f";end | sort -u -k1,1)

  # If a plugin name was specified, output the bare version number (or nothing
  # if no version is set). Otherwise, output the full list of declarations
  if set -q plugin_name[1]
    for entry in $plugin_version_array
      if string match -q "$plugin_name*" "$entry"
        echo (string split " " "$entry")[2]
      end
    end
  else
    for entry in $plugin_version_array;echo "$entry";end
  end
end
