function is_remote_session -d 'Check if the current fish session is over SSH'
  if set -q SSH_CONNECTION
    or set -q SSH_TTY
    or set -q SSH_CLIENT
    or string match -q '*ssh*' (ps -o comm= -p $fish_pid)
    return 0
  end
  return 1
end

